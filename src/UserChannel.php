<?php

namespace Antonankilov\Channel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserChannel extends Model
{
    protected $table = 'user_channel';
    public $timestamps = false;
    protected $with = ['channel'];
    protected $fillable = ['user_id', 'channel_id'];

    public function scopeCurrentUser($query)
    {
        $userId = Auth::id();
        return $query->where('user_id', '=', $userId);
    }

    public function channel()
    {
        return $this->morphTo();
    }
}
