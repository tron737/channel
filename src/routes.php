<?php

Route::resource('channels', 'Antonankilov\Channel\Controllers\ChannelController', ['only' => [
    'index', 'store'
]]);