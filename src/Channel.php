<?php

namespace Antonankilov\Channel;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    public $timestamps = false;

    public function channelUser()
    {
        return $this->morphOne('Antonankilov\Channel\UserChannel', 'channel');
    }
}
