<?php

namespace Antonankilov\Channel\Controllers;

use Antonankilov\Channel\Channel;
use Antonankilov\Channel\Requests\StoreUserChannel;
use Antonankilov\Channel\UserChannel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChannelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $channels = Channel::with(['channelUser' => function ($query) {
            return $query->CurrentUser();
        }])->paginate();

        $lastPage = $channels->lastPage();
        $currentPage = $channels->currentPage();
        return view('channel::checkboxes', compact('channels', 'lastPage', 'currentPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param StoreUserChannel $request
     */
    public function store(StoreUserChannel $request)
    {
        $userId = Auth::id();
        $channelId = $request->get('id');

        $userChannel = UserChannel::create([
            'channel_id' => $channelId,
            'user_id' => $userId
        ]);

        return $userChannel;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
