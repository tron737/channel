<?php

namespace Antonankilov\Channel;

use Illuminate\Support\ServiceProvider;

class ChannelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'channel');

        $this->publishes([
            __DIR__.'/views' => resource_path('views/vendor/courier'),
        ]);

        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Antonankilov\Channel\Controllers\ChannelController');
        $this->app->register('TwigBridge\ServiceProvider');
    }
}
